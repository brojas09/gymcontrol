--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.11
-- Dumped by pg_dump version 9.4.11
-- Started on 2018-10-29 11:09:46

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

DROP DATABASE gym_control;
--
-- TOC entry 2052 (class 1262 OID 83890)
-- Name: gym_control; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE gym_control WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Spanish_Paraguay.1252' LC_CTYPE = 'Spanish_Paraguay.1252';


ALTER DATABASE gym_control OWNER TO postgres;

--\connect gym_control

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 1 (class 3079 OID 11855)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2055 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 174 (class 1259 OID 84560)
-- Name: estudiantes; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE estudiantes (
    id_estudiante integer NOT NULL,
    nombre_apellido character varying(120),
    direccion character varying(120),
    telefono character varying(20),
    email character varying(50),
    ruc character varying(20),
    estado integer,
    id_gimnasio integer,
    fecha_proximo_pago date
);


ALTER TABLE estudiantes OWNER TO postgres;

--
-- TOC entry 179 (class 1259 OID 84649)
-- Name: estudiantes_id_estudiante_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE estudiantes_id_estudiante_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE estudiantes_id_estudiante_seq OWNER TO postgres;

--
-- TOC entry 2056 (class 0 OID 0)
-- Dependencies: 179
-- Name: estudiantes_id_estudiante_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE estudiantes_id_estudiante_seq OWNED BY estudiantes.id_estudiante;


--
-- TOC entry 176 (class 1259 OID 84574)
-- Name: gimnasios; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE gimnasios (
    id_gimnasio integer NOT NULL,
    razon_social character varying(100),
    direccion character varying(120),
    telefono character varying(20),
    email character varying(100),
    ruc character varying(20),
    estado integer
);


ALTER TABLE gimnasios OWNER TO postgres;

--
-- TOC entry 175 (class 1259 OID 84572)
-- Name: gimnasios_id_gimnasio_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE gimnasios_id_gimnasio_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE gimnasios_id_gimnasio_seq OWNER TO postgres;

--
-- TOC entry 2057 (class 0 OID 0)
-- Dependencies: 175
-- Name: gimnasios_id_gimnasio_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE gimnasios_id_gimnasio_seq OWNED BY gimnasios.id_gimnasio;


--
-- TOC entry 178 (class 1259 OID 84582)
-- Name: producto; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE producto (
    id_producto integer NOT NULL,
    nombre character varying(50),
    descripcion character varying(150),
    precio_unitario bigint,
    cantidad integer,
    minimo integer,
    id_gimnasio integer,
    tipo_producto integer
);


ALTER TABLE producto OWNER TO postgres;

--
-- TOC entry 177 (class 1259 OID 84580)
-- Name: producto_id_producto_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE producto_id_producto_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE producto_id_producto_seq OWNER TO postgres;

--
-- TOC entry 2058 (class 0 OID 0)
-- Dependencies: 177
-- Name: producto_id_producto_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE producto_id_producto_seq OWNED BY producto.id_producto;


--
-- TOC entry 180 (class 1259 OID 84669)
-- Name: usuarios_id_usuario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE usuarios_id_usuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE usuarios_id_usuario_seq OWNER TO postgres;

--
-- TOC entry 173 (class 1259 OID 84550)
-- Name: usuarios; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE usuarios (
    id_usuario integer DEFAULT nextval('usuarios_id_usuario_seq'::regclass) NOT NULL,
    nombre_usuario character varying(100),
    passwd character varying(100),
    estado integer,
    fecha_acceso timestamp with time zone,
    nombre character varying(50),
    nivel integer,
    id_gimnasio integer
);


ALTER TABLE usuarios OWNER TO postgres;

--
-- TOC entry 184 (class 1259 OID 86951)
-- Name: venta_det; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE venta_det (
    id_venta_det integer NOT NULL,
    id_venta integer,
    id_producto integer,
    cantidad integer,
    usr_ins integer,
    fecha_ins timestamp with time zone,
    usr_upd integer,
    fecha_upd timestamp with time zone,
    valor_venta bigint
);


ALTER TABLE venta_det OWNER TO postgres;

--
-- TOC entry 183 (class 1259 OID 86949)
-- Name: venta_det_id_venta_det_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE venta_det_id_venta_det_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE venta_det_id_venta_det_seq OWNER TO postgres;

--
-- TOC entry 2059 (class 0 OID 0)
-- Dependencies: 183
-- Name: venta_det_id_venta_det_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE venta_det_id_venta_det_seq OWNED BY venta_det.id_venta_det;


--
-- TOC entry 182 (class 1259 OID 86925)
-- Name: ventas; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ventas (
    id_venta integer NOT NULL,
    id_gimnasio integer,
    id_estudiante integer,
    id_usuario integer,
    fecha_emision timestamp with time zone,
    estado integer,
    total bigint,
    observacion character varying(250),
    usr_modificacion integer,
    fecha_modificacion timestamp with time zone
);


ALTER TABLE ventas OWNER TO postgres;

--
-- TOC entry 181 (class 1259 OID 86923)
-- Name: ventas_id_venta_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE ventas_id_venta_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ventas_id_venta_seq OWNER TO postgres;

--
-- TOC entry 2060 (class 0 OID 0)
-- Dependencies: 181
-- Name: ventas_id_venta_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE ventas_id_venta_seq OWNED BY ventas.id_venta;


--
-- TOC entry 1912 (class 2604 OID 84651)
-- Name: id_estudiante; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY estudiantes ALTER COLUMN id_estudiante SET DEFAULT nextval('estudiantes_id_estudiante_seq'::regclass);


--
-- TOC entry 1913 (class 2604 OID 84577)
-- Name: id_gimnasio; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY gimnasios ALTER COLUMN id_gimnasio SET DEFAULT nextval('gimnasios_id_gimnasio_seq'::regclass);


--
-- TOC entry 1914 (class 2604 OID 84585)
-- Name: id_producto; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY producto ALTER COLUMN id_producto SET DEFAULT nextval('producto_id_producto_seq'::regclass);


--
-- TOC entry 1916 (class 2604 OID 86954)
-- Name: id_venta_det; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY venta_det ALTER COLUMN id_venta_det SET DEFAULT nextval('venta_det_id_venta_det_seq'::regclass);


--
-- TOC entry 1915 (class 2604 OID 86928)
-- Name: id_venta; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ventas ALTER COLUMN id_venta SET DEFAULT nextval('ventas_id_venta_seq'::regclass);


--
-- TOC entry 1920 (class 2606 OID 84656)
-- Name: pk_estudiante; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY estudiantes
    ADD CONSTRAINT pk_estudiante PRIMARY KEY (id_estudiante);


--
-- TOC entry 1922 (class 2606 OID 84579)
-- Name: pk_gimnasio; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY gimnasios
    ADD CONSTRAINT pk_gimnasio PRIMARY KEY (id_gimnasio);


--
-- TOC entry 1924 (class 2606 OID 84587)
-- Name: pk_producto; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY producto
    ADD CONSTRAINT pk_producto PRIMARY KEY (id_producto);


--
-- TOC entry 1918 (class 2606 OID 84658)
-- Name: pk_usuario; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY usuarios
    ADD CONSTRAINT pk_usuario PRIMARY KEY (id_usuario);


--
-- TOC entry 1928 (class 2606 OID 86956)
-- Name: pk_venta_det; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY venta_det
    ADD CONSTRAINT pk_venta_det PRIMARY KEY (id_venta_det);


--
-- TOC entry 1926 (class 2606 OID 86930)
-- Name: pk_ventas; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ventas
    ADD CONSTRAINT pk_ventas PRIMARY KEY (id_venta);


--
-- TOC entry 1933 (class 2606 OID 86936)
-- Name: fk_estudiante; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ventas
    ADD CONSTRAINT fk_estudiante FOREIGN KEY (id_estudiante) REFERENCES estudiantes(id_estudiante);


--
-- TOC entry 1929 (class 2606 OID 86908)
-- Name: fk_gimnasio; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuarios
    ADD CONSTRAINT fk_gimnasio FOREIGN KEY (id_gimnasio) REFERENCES gimnasios(id_gimnasio);


--
-- TOC entry 1930 (class 2606 OID 86913)
-- Name: fk_gimnasio; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY estudiantes
    ADD CONSTRAINT fk_gimnasio FOREIGN KEY (id_gimnasio) REFERENCES gimnasios(id_gimnasio);


--
-- TOC entry 1931 (class 2606 OID 86918)
-- Name: fk_gimnasio; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY producto
    ADD CONSTRAINT fk_gimnasio FOREIGN KEY (id_gimnasio) REFERENCES gimnasios(id_gimnasio);


--
-- TOC entry 1932 (class 2606 OID 86931)
-- Name: fk_gimnasio; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ventas
    ADD CONSTRAINT fk_gimnasio FOREIGN KEY (id_gimnasio) REFERENCES gimnasios(id_gimnasio);


--
-- TOC entry 1937 (class 2606 OID 86962)
-- Name: fk_producto; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY venta_det
    ADD CONSTRAINT fk_producto FOREIGN KEY (id_producto) REFERENCES producto(id_producto);


--
-- TOC entry 1935 (class 2606 OID 86972)
-- Name: fk_usr_upd; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ventas
    ADD CONSTRAINT fk_usr_upd FOREIGN KEY (usr_modificacion) REFERENCES usuarios(id_usuario);


--
-- TOC entry 1934 (class 2606 OID 86941)
-- Name: fk_usuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ventas
    ADD CONSTRAINT fk_usuario FOREIGN KEY (id_usuario) REFERENCES usuarios(id_usuario);


--
-- TOC entry 1938 (class 2606 OID 86967)
-- Name: fk_usuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY venta_det
    ADD CONSTRAINT fk_usuario FOREIGN KEY (usr_ins) REFERENCES usuarios(id_usuario);


--
-- TOC entry 1936 (class 2606 OID 86957)
-- Name: fk_venta; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY venta_det
    ADD CONSTRAINT fk_venta FOREIGN KEY (id_venta) REFERENCES ventas(id_venta);


--
-- TOC entry 2054 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2018-10-29 11:09:47

--
-- PostgreSQL database dump complete
--

