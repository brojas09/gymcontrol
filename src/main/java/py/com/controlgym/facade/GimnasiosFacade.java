/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.controlgym.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import py.com.controlgym.entities.Gimnasios;

/**
 *
 * @author broja
 */
@Stateless
public class GimnasiosFacade extends AbstractFacade<Gimnasios> {

    @PersistenceContext(unitName = "com.gymcontrol_gymcontrol_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public GimnasiosFacade() {
        super(Gimnasios.class);
    }

    public Gimnasios find(Integer id) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.gymcontrol_gymcontrol_war_1.0-SNAPSHOTPU");
        em = emf.createEntityManager();
        return em.find(Gimnasios.class, id);
    }

}
