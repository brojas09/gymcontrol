/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.controlgym.facade;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import py.com.controlgym.entities.Estudiantes;

/**
 *
 * @author broja
 */
@Stateless
public class EstudiantesFacade extends AbstractFacade<Estudiantes> {

    @PersistenceContext(unitName = "com.gymcontrol_gymcontrol_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EstudiantesFacade() {
        super(Estudiantes.class);
    }

    public List<Estudiantes> findAll(Integer idGimnasio) {
        Query q = getEntityManager().createNamedQuery("Estudiantes.getEstudiantesByGimnasio");
        q.setParameter("idGimnasio", idGimnasio);
        if (q.getResultList().isEmpty() != true) {
            return q.getResultList();
        } else {
            return null;
        }
    }

    public Estudiantes find(Integer id) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.gymcontrol_gymcontrol_war_1.0-SNAPSHOTPU");
        em = emf.createEntityManager();
        return em.find(Estudiantes.class, id);
    }

    public List<Estudiantes> findAllActivos(Integer idGimnasio) {
        Query q = getEntityManager().createNamedQuery("Estudiantes.getEstudiantesActivosByGimnasio");
        q.setParameter("idGimnasio", idGimnasio);
        if (q.getResultList().isEmpty() != true) {
            return q.getResultList();
        } else {
            return null;
        }
    }

    public List<Estudiantes> findCuotasPorVencer(Integer idGimnasio) {
        Query q;
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(new Date());
        calendar.add(Calendar.DATE, 5);//add 5 days
        Date fechaValidacion = calendar.getTime();
        if (idGimnasio > 0) {
            q = getEntityManager().createNamedQuery("Estudiantes.getEstudiantesPorVencerConGym");
            q.setParameter("fechaValidacion", fechaValidacion);
            q.setParameter("idGimnasio", idGimnasio);

        } else {
            q = getEntityManager().createNamedQuery("Estudiantes.getEstudiantesPorVencerSinGym");
            q.setParameter("fechaValidacion", fechaValidacion);

        }
        if (q.getResultList().isEmpty() != true) {
            return q.getResultList();
        } else {
            return null;
        }
    }
}
