/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.controlgym.facade;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import py.com.controlgym.entities.Asistencia;

/**
 *
 * @author broja
 */
@Stateless
public class AsistenciaFacade extends AbstractFacade<Asistencia>{

    @PersistenceContext(unitName = "com.gymcontrol_gymcontrol_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AsistenciaFacade() {
        super(Asistencia.class);
    }

    public Asistencia find(Integer id) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.gymcontrol_gymcontrol_war_1.0-SNAPSHOTPU");
        em = emf.createEntityManager();
        return em.find(Asistencia.class, id);
    }

    public boolean AsistioHoy(Integer idEstudiante) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String fecha= dateFormat.format(new Date());
        String query="select count (*) from asistencia where fecha_hora between '#FECHA# 00:00:00' and '#FECHA# 23:59:00' and id_alumno= #IDALUMNO#";
        query=query.replace("#FECHA#", fecha);
        query=query.replace("#IDALUMNO#", idEstudiante.toString());
        int num = ((Number)em.createNativeQuery(query)
                    .getSingleResult()).intValue();
        
        return num>0;  
    }
}
