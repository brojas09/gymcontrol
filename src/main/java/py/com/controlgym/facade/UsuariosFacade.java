/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.controlgym.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import py.com.controlgym.entities.Usuarios;

/**
 *
 * @author broja
 */
@Stateless
public class UsuariosFacade extends AbstractFacade<Usuarios> {

    @PersistenceContext(unitName = "com.gymcontrol_gymcontrol_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuariosFacade() {
        super(Usuarios.class);
    }

    public Usuarios findByUsuario(String user) {
        Query q = getEntityManager().createNamedQuery("Usuarios.findByUsuario");
        q.setParameter("nombreUsuario", user);
        if (q.getResultList().isEmpty() != true) {
            return (Usuarios) q.getResultList().get(0);
        } else {
            return null;
        }
    }

    public Usuarios find(Integer id) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.gymcontrol_gymcontrol_war_1.0-SNAPSHOTPU");
        em = emf.createEntityManager();
        return em.find(Usuarios.class, id);
    }

    public int updatePass(Usuarios u) {
        em.joinTransaction();
        try {
            String query = "Update Usuarios set passwd = '" + u.getPasswd() + "' where id_usuario = " + u.getIdUsuario();
            System.out.println(query);
            Query q = getEntityManager().createNativeQuery(query);
            int result = q.executeUpdate();
            em.clear();
            return result;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return -1;
        }
    }
}
