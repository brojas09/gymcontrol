package py.com.controlgym.controller;

import py.com.controlgym.entities.Estudiantes;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.event.ActionEvent;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import py.com.controlgym.controller.login.LoginBean;
import py.com.controlgym.entities.Asistencia;
import py.com.controlgym.entities.Gimnasios;
import py.com.controlgym.facade.AsistenciaFacade;
import py.com.controlgym.facade.EstudiantesFacade;
import py.com.controlgym.facade.GimnasiosFacade;

@Named(value = "estudiantesController")
@ViewScoped
public class EstudiantesController extends AbstractController<Estudiantes> implements Serializable {

    public EstudiantesController() {
        super(Estudiantes.class);
    }

    @EJB
    private GimnasiosFacade gimnasiosFacade;
    @EJB
    private EstudiantesFacade estudiantesFacade;
    @EJB
    private AsistenciaFacade asistenciaFacade;

    private final LoginBean loginBean = new LoginBean();
    private boolean activo;
    private List<Estudiantes> estudiantesActivos;
    private List<Estudiantes> porVencerList;

    public Collection<Estudiantes> getPorVencerList() {
        Integer idGimnasio = loginBean.getIdGimnasio();
        porVencerList = estudiantesFacade.findCuotasPorVencer(idGimnasio);
        return porVencerList;
    }

    public void setPorVencerList(List<Estudiantes> porVencerList) {
        this.porVencerList = porVencerList;
    }

    public Collection<Estudiantes> getEstudiantesActivos() {
        if (loginBean.tieneRol()) {
            return super.getItems();
        } else {
            Integer idGimnasio = loginBean.getIdGimnasio();
            estudiantesActivos = estudiantesFacade.findAllActivos(idGimnasio);
            return estudiantesActivos;
        }
    }

    public void setEstudiantesActivos(List<Estudiantes> estudiantesActivos) {
        this.estudiantesActivos = estudiantesActivos;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    @Override
    public Collection<Estudiantes> getItems() {
        if (loginBean.tieneRol()) {
            return super.getItems();
        } else {
            Integer idGimnasio = loginBean.getIdGimnasio();
            return estudiantesFacade.findAll(idGimnasio);
        }
    }

    @Override
    public void saveNew(ActionEvent event) {
        super.getSelected().setEstado(1);
        if (!loginBean.tieneRol()) {
            LoadGimnasio();
        }
        super.saveNew(event);
    }

    @Override
    public void save(ActionEvent event) {
        if (activo) {
            super.getSelected().setEstado(1);
        } else {
            super.getSelected().setEstado(0);
        }
        /*if (!loginBean.tieneRol()) {
            LoadGimnasio();
        }*/
        super.save(event);
    }

    public void prepareEdit(ActionEvent event) {
        activo = this.getSelected().getEstado() == 1;
    }
    
    public void MarcarAsistencia(Estudiantes estudiante) {
        Asistencia asistencia= new Asistencia();
        asistencia.setIdAlumno(estudiante);
        asistencia.setFechaHora(new Date());
        asistenciaFacade.create(asistencia);
    }
    public Boolean bloquearPresente(Estudiantes estudiante) {
        //return true;
       return asistenciaFacade.AsistioHoy(estudiante.getIdEstudiante());
    }
    private void LoadGimnasio() {
        Integer idGimnasio = loginBean.getIdGimnasio();
        Gimnasios g = gimnasiosFacade.find(idGimnasio);
        super.getSelected().setIdGimnasio(g);
    }
}
