package py.com.controlgym.controller;

import py.com.controlgym.entities.Producto;
import java.io.Serializable;
import java.util.Collection;
import javax.ejb.EJB;
import javax.faces.event.ActionEvent;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import py.com.controlgym.controller.login.LoginBean;
import py.com.controlgym.entities.Gimnasios;
import py.com.controlgym.facade.GimnasiosFacade;
import py.com.controlgym.facade.ProductoFacade;

@Named(value = "productoController")
@ViewScoped
public class ProductoController extends AbstractController<Producto> implements Serializable {

    public ProductoController() {
        super(Producto.class);
    }
    @EJB
    private GimnasiosFacade gimnasiosFacade;
    @EJB
    private ProductoFacade productoFacade;
    
    private final LoginBean loginBean = new LoginBean();

    @Override
    public void saveNew(ActionEvent event) {
        if (!loginBean.tieneRol()) {
            LoadGimnasio();
        }
        super.saveNew(event);
    }

    @Override
    public Collection<Producto> getItems() {
        if (loginBean.tieneRol()) {
            return super.getItems();
        } else {
            Integer idGimnasio = loginBean.getIdGimnasio();
            return productoFacade.findAll(idGimnasio);
        }
    }

    public String evalTipo(String valor) {
        int valorNum = Integer.parseInt(valor);
        String tipo = "";
        switch (valorNum) {
            case 1:
                tipo = "Mercaderia";
                break;
            case 2:
                tipo = "Cuota";
                break;
            case 3:
                tipo = "Promo 30%";
                break;
            case 4:
                tipo = "Promo 2x1";
                break;
            case 5:
                tipo = "Promo 3x2";
                break;
            case 6:
                tipo = "Promo Mes Gratis";
                break;
        }
        return tipo;
    }

    private void LoadGimnasio() {
        Integer idGimnasio = loginBean.getIdGimnasio();
        Gimnasios g = gimnasiosFacade.find(idGimnasio);
        super.getSelected().setIdGimnasio(g);
    }
}
