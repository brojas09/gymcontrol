package py.com.controlgym.controller;

import py.com.controlgym.entities.Usuarios;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import py.com.controlgym.controller.login.LoginBean;
import py.com.controlgym.controller.util.HashTextTest;
import py.com.controlgym.facade.UsuariosFacade;

@Named(value = "usuariosController")
@ViewScoped
public class UsuariosController extends AbstractController<Usuarios> implements Serializable {

    @EJB
    private UsuariosFacade ejbFacade;
    private boolean activo;

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public UsuariosController() {
        super(Usuarios.class);
    }

    public String evalEstado(String valor) {
        int valorNum = Integer.parseInt(valor);
        String estado;
        if (valorNum == 1) {
            estado = "Activo";
        } else {
            estado = "Inactivo";
        }
        return estado;
    }

    public String evalNivel(String valor) {
        int valorNum = Integer.parseInt(valor);
        String nivel;
        switch (valorNum) {
            case 1:
                nivel = "Administrador";
                break;
            case 2:
                nivel = "Propietario";
                break;
            default:
                nivel = "Encargado";
                break;
        }
        return nivel;
    }

    @Override
    public void saveNew(ActionEvent event) {
        super.getSelected().setEstado(1);
        super.getSelected().setPasswd(HashTextTest.sha1(super.getSelected().getPasswd()));
        super.saveNew(event);
    }

    @Override
    public void save(ActionEvent event) {
        if (activo) {
            super.getSelected().setEstado(1);
        } else {
            super.getSelected().setEstado(0);
        }
        super.save(event);
    }

    public void prepareEdit(ActionEvent event) {
        activo = this.getSelected().getEstado() == 1;
    }
    private String newPass1;
    private String newPass2;
    private String oldPass;

    public String getNewPass1() {
        return newPass1;
    }

    public void setNewPass1(String newPass1) {
        this.newPass1 = newPass1;
    }

    public String getNewPass2() {
        return newPass2;
    }

    public void setNewPass2(String newPass2) {
        this.newPass2 = newPass2;
    }

    public String getOldPass() {
        return oldPass;
    }

    public void setOldPass(String oldPass) {
        this.oldPass = oldPass;
    }

    public void updatePass(ActionEvent event) {
        FacesMessage message;
        FacesContext context = FacesContext.getCurrentInstance();
        if (newPass1.matches(newPass2)) {
            //recuperar usuario
            LoginBean lb = new LoginBean();
            Usuarios u = ejbFacade.find(lb.getUsuario());
            //comparar contraseña actual
            if (u.getPasswd().matches(HashTextTest.sha1(oldPass))) {
                u.setPasswd(HashTextTest.sha1(newPass1));
                //actualizar bd
                int i = ejbFacade.updatePass(u);
                System.out.println(i);
                if (i == 1) {
                    message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Éxito", "Actualización realizada con éxito");
                    context.addMessage(null, message);

                } else {
                    message = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", "No se pudo actualizar en la Base de Datos");
                    context.addMessage(null, message);

                }

                //RequestContext.getCurrentInstance().showMessageInDialog(message);
            } else {
                message = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", "Las contraseña anterior no coincide. Intente de nuevo");
                context.addMessage(null, message);
                //RequestContext.getCurrentInstance().showMessageInDialog(message);
            }
        } else {
            message = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", "Las contraseñas no coinciden. Intente de nuevo");
            context.addMessage(null, message);
            //RequestContext.getCurrentInstance().showMessageInDialog(message);
        }
    }
}
