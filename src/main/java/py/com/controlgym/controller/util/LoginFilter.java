package py.com.controlgym.controller.util;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import py.com.controlgym.controller.login.LoginBean;

/**
 *
 * @author Abby
 */
public class LoginFilter implements Filter {

    public static final String LOGIN_PAGE = "/login.xhtml";

    @Override
    public void doFilter(ServletRequest servletRequest,
            ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {

        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;

        // managed bean name is exactly the session attribute name
        boolean estaLogueado = (boolean) httpServletRequest
                .getSession().getAttribute("estaLogueado");

        if (estaLogueado) {
            //if (userManager.estaLogeado()) {
            System.out.println("El usuario está logueado!!");
            // user is logged in, continue request
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            System.out.println("El usuario NO está logueado!!");
            // user is not logged in, redirect to login page
            httpServletResponse.sendRedirect(httpServletRequest
                    .getContextPath() + LOGIN_PAGE);
        }
    }

    @Override
    public void init(FilterConfig arg0) throws ServletException {
        System.out.println("LoginFilter Inicializada!!");
    }

    @Override
    public void destroy() {
        System.out.println("LoginFilter Destruida!!");
    }
}
