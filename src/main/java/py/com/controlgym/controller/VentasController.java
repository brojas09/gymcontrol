package py.com.controlgym.controller;

import py.com.controlgym.entities.Ventas;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import javax.ejb.EJB;
import javax.faces.event.ActionEvent;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import py.com.controlgym.controller.login.LoginBean;
import py.com.controlgym.entities.Estudiantes;
import py.com.controlgym.entities.Gimnasios;
import py.com.controlgym.entities.Producto;
import py.com.controlgym.entities.VentaDet;
import py.com.controlgym.facade.EstudiantesFacade;
import py.com.controlgym.facade.GimnasiosFacade;
import py.com.controlgym.facade.ProductoFacade;
import py.com.controlgym.facade.VentasFacade;
import py.com.controlgym.facade.UsuariosFacade;

@Named(value = "ventasController")
@ViewScoped
public class VentasController extends AbstractController<Ventas> implements Serializable {

    @EJB
    private GimnasiosFacade gimnasiosFacade;

    @EJB
    private EstudiantesFacade estudiantesFacade;

    @EJB
    private UsuariosFacade usuariosFacade;

    @EJB
    private VentasFacade ventasFacade;

    @EJB
    private ProductoFacade productoFacade;
    private boolean requiereCantidad=true;
    private VentaDet detalle;
    private Producto producto;
    private final LoginBean loginBean = new LoginBean();

    public VentasController() {
        super(Ventas.class);
    }

    public VentaDet getDetalle() {
        return detalle;
    }

    public void setDetalle(VentaDet detalle) {
        this.detalle = detalle;
    }

    @Override
    public Collection<Ventas> getItems() {
        if (loginBean.tieneRol()) {
            return super.getItems();
        } else {
            Integer idGimnasio = loginBean.getIdGimnasio();
            return ventasFacade.findAll(idGimnasio);
        }
    }
    private Ventas venta;

    @Override
    public Ventas prepareCreate(ActionEvent event) {
        venta = super.prepareCreate(event);
        venta.setIdGimnasio(LoadGimnasio());
        venta.setEstado(1);
        venta.setFechaEmision(new Date());
        venta.setIdUsuario(usuariosFacade.find(loginBean.getUsuario()));
        venta.setVentaDetList(new ArrayList<VentaDet>());
        return venta;
    }

    @Override
    public void saveNew(ActionEvent event) {
        super.saveNew(event);
        for (VentaDet det : venta.getVentaDetList()) {
            int tipo = det.getIdProducto().getTipoProducto();
            switch (tipo) {
                //Actualizar Stock de Productos
                case 1:
                    Producto p = det.getIdProducto();
                    p.setCantidad(p.getCantidad() - det.getCantidad());
                    //Aca validar minimo y alertar
                    productoFacade.edit(p);
                    break;
                // Actualizar Cuotas // CASE 4 Siguiente vuelta
                case 2:
                case 3:
                case 6:
                    Calendar cal = Calendar.getInstance();
                    cal.add(Calendar.MONTH, 1);
                    Estudiantes e = super.getSelected().getIdEstudiante();
                    e.setFechaProximoPago(cal.getTime());
                    estudiantesFacade.edit(e);
                    break;
                case 5:// Promo 3x2
                    cal = Calendar.getInstance();
                    cal.add(Calendar.MONTH, 3);
                    e = super.getSelected().getIdEstudiante();
                    e.setFechaProximoPago(cal.getTime());
                    estudiantesFacade.edit(e);
                    break;
            }
        }
    }
    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }
    public String evalEstado(String valor) {
        int valorNum = Integer.parseInt(valor);
        String estado;
        if (valorNum == 1) {
            estado = "Activo";
        } else {
            estado = "Anulado";
        }
        return estado;
    }

    public void onProductoSelect() {
        requiereCantidad=true;
        detalle = new VentaDet();
        detalle.setIdProducto(producto);
        if(producto.getTipoProducto()==1){
            requiereCantidad=false;
        }else{
            detalle.setCantidad(1);
        }
    }

    public boolean isRequiereCantidad() {
        return requiereCantidad;
    }

    public void setRequiereCantidad(boolean requiereCantidad) {
        this.requiereCantidad = requiereCantidad;
    }
    public void addItemDetalles() {
        detalle.setIdVenta(this.getSelected());
        BigInteger cantidad = BigInteger.ONE;
        if (detalle.getIdProducto().getTipoProducto() == 1) {
            cantidad = new BigInteger(detalle.getCantidad().toString());
        }
        BigInteger importeActual = super.getSelected().getTotal()==null? BigInteger.ZERO:super.getSelected().getTotal();
        cantidad=cantidad.multiply(detalle.getIdProducto().getPrecioUnitario());
        importeActual=importeActual.add(cantidad);
        detalle.setValorVenta(cantidad);
        super.getSelected().getVentaDetList().add(detalle);
        super.getSelected().setTotal(importeActual);                       
    }

    public void removeItemDetalles(VentaDet item) {
        this.getSelected().setTotal(
                this.getSelected().getTotal().subtract(item.getValorVenta()));
        this.getSelected().getVentaDetList().remove(item);
    }

    private Gimnasios LoadGimnasio() {
        Integer idGimnasio = loginBean.getIdGimnasio();
        Gimnasios g = gimnasiosFacade.find(idGimnasio);
        return g;
    }
}
