package py.com.controlgym.controller;

import py.com.controlgym.entities.Gimnasios;
import java.io.Serializable;
import javax.faces.event.ActionEvent;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

@Named(value = "gimnasiosController")
@ViewScoped
public class GimnasiosController extends AbstractController<Gimnasios> implements Serializable {

    public GimnasiosController() {
        super(Gimnasios.class);
    }

    public String evalEstado(String valor) {
        int valorNum = Integer.parseInt(valor);
        String estado;
        if (valorNum == 1) {
            estado = "Activo";
        } else {
            estado = "Inactivo";
        }
        return estado;
    }

    @Override
    public void saveNew(ActionEvent event) {
        super.getSelected().setEstado(1);
        super.saveNew(event);
    }
    
    @Override
    public void save(ActionEvent event) {
        super.save(event);
    }
}
