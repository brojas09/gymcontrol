/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.controlgym.controller.login;

import py.com.controlgym.controller.util.HashTextTest;
import py.com.controlgym.entities.Usuarios;
import py.com.controlgym.facade.UsuariosFacade;
import java.io.IOException;
import java.io.Serializable;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
//import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.inject.Named;

import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
/**
 *
 * @author broja
 */
@ManagedBean
@SessionScoped
@Named(value = "loginBean")

public class LoginBean implements Serializable {

    @Inject
    private UsuariosFacade usuarioFacade;

    public LoginBean() {
    }
    private boolean logeado = false;
    private String pass, user, nombreUser, usuarioUser;

    public boolean isLogeado() {
        return logeado;
    }

    public void setLogeado(boolean logeado) {
        this.logeado = logeado;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getNombreUser() {
        return nombreUser;
    }

    public void setNombreUser(String nombreUser) {
        this.nombreUser = nombreUser;
    }

    public String getUsuarioUser() {
        return usuarioUser;
    }

    public void setUsuarioUser(String usuarioUser) {
        this.usuarioUser = usuarioUser;
    }

    public String isLoggedInForwardHome() {
        if (logeado) {
            try {
                this.redirect(this.getContextPath() + "/index.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(LoginBean.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;
        }
        return null;
    }
    Usuarios usuario;

    public String login(ActionEvent event) throws IOException {
        FacesMessage message;
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getFlash().setKeepMessages(true);

        nombreUser = usuarioUser = "";
        usuario = null;
        try {
            usuario = usuarioFacade.findByUsuario(user);
        } catch (Exception e) {
            System.out.println(e);
            usuario = null;
        }
        if (usuario != null) {
            if ((usuario.getPasswd().compareTo(HashTextTest.sha1(pass)) == 0)
                    && (usuario.getNivel() == 1))//context.execute("swal('Correcto', 'Usted ha ingresado correctamente al sistema', 'success')");
            {
                logeado = true;
                message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Bienvenido "+ usuario.getNombre(), "");
                FacesContext.getCurrentInstance().addMessage(null, message);
                nombreUser = usuario.getNombre();
                usuarioUser = usuario.getNombreUsuario();
            } else if ((usuario.getPasswd().compareTo(HashTextTest.sha1(pass)) == 0)
                    && (usuario.getIdGimnasio().getEstado() == 1)) {
                logeado = true;
                message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Bienvenido "+ usuario.getNombre(), "");
                FacesContext.getCurrentInstance().addMessage(null, message);
                nombreUser = usuario.getNombre();
                usuarioUser = usuario.getNombreUsuario();
            } else if (usuario.getIdGimnasio().getEstado() != 1) {
                message = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Gimnasio Inválido", "Favor ponerse en contacto con el Administrador");
                FacesContext.getCurrentInstance().addMessage(null, message);
                return null;
            } else {
                message = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error de Login", "Credenciales Invalidas");
                FacesContext.getCurrentInstance().addMessage(null, message);
                return null;
            }
        } else {
            message = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error de Login", "Usuario no Existe");
            FacesContext.getCurrentInstance().addMessage(null, message);
            return null;
        }

        if (logeado) {
            this.addSessionParameter("usuario", usuario.getNombre());
            this.addSessionParameter("username", usuario.getNombreUsuario());
            this.addSessionParameter("idUser", usuario.getIdUsuario());
            this.addSessionParameter("estaLogueado", logeado);
            this.addSessionParameter("nivel", usuario.getNivel());
            if (usuario.getNivel() > 1) {
                this.addSessionParameter("gimnasio", usuario.getIdGimnasio().getIdGimnasio());
            }

            usuario.setFechaAcceso(Calendar.getInstance().getTime());
            usuarioFacade.edit(usuario);
            String pagina = this.getContextPath() + "/index.xhtml";
            try {
                this.redirect(pagina);
            } catch (IOException ex) {
                //LOGEO LOG ANTERIOR
                //           Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;
        }
        return null;
    }

    public boolean estaLogeado() {
        return logeado;
    }

    public void logout() {
        //Se elimina todos los objetos asociados a la sesion
        this.getHttpRequest().getSession().invalidate();
        logeado = false;
        user = "";
        try {
            this.redirect(this.getContextPath() + "/login.xhtml");
            System.out.println("Sesión cerrada con EXITO!");
        } catch (IOException ex) {
            Logger.getLogger(LoginBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    protected void redirect(String url) throws IOException {
        FacesContext.getCurrentInstance().getExternalContext().redirect(url);
    }

    protected Object getSessionParameter(String key) {
        return this.getHttpRequest().getSession().getAttribute(key);
    }

    protected void addSessionParameter(String key, Object value) {
        this.getHttpRequest().getSession().setAttribute(key, value);
    }

    protected String getContextPath() {
        return this.getHttpRequest().getContextPath();
    }

    protected HttpServletRequest getHttpRequest() {
        try {
            return (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        } catch (Exception ex) {
        }
        return null;
    }

    public Integer getUsuario() {
        javax.servlet.http.HttpSession session = this.getHttpRequest().getSession();
        Integer idUser = Integer.parseInt(session.getAttribute("idUser").toString());
        return idUser;
    }

    public Boolean tieneRol() {
        javax.servlet.http.HttpSession session = this.getHttpRequest().getSession();
        Integer nivel = Integer.parseInt(session.getAttribute("nivel").toString());
        return (nivel == 1);
    }

    public Integer getIdGimnasio() {
        javax.servlet.http.HttpSession session = this.getHttpRequest().getSession();
        try{
            Integer idGimnasio = Integer.parseInt(session.getAttribute("gimnasio").toString());
            return idGimnasio;
        }catch(Exception e){
            return 0;
        }
    }
}
