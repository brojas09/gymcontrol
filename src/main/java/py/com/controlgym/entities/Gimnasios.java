/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package py.com.controlgym.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * @author broja
 */
@Entity
@Table(name = "gimnasios", catalog = "gym_control", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Gimnasios.findAll", query = "SELECT g FROM Gimnasios g"),
    @NamedQuery(name = "Gimnasios.findByIdGimnasio", query = "SELECT g FROM Gimnasios g WHERE g.idGimnasio = :idGimnasio"),
    @NamedQuery(name = "Gimnasios.findByRazonSocial", query = "SELECT g FROM Gimnasios g WHERE g.razonSocial = :razonSocial"),
    @NamedQuery(name = "Gimnasios.findByDireccion", query = "SELECT g FROM Gimnasios g WHERE g.direccion = :direccion"),
    @NamedQuery(name = "Gimnasios.findByTelefono", query = "SELECT g FROM Gimnasios g WHERE g.telefono = :telefono"),
    @NamedQuery(name = "Gimnasios.findByEmail", query = "SELECT g FROM Gimnasios g WHERE g.email = :email"),
    @NamedQuery(name = "Gimnasios.findByRuc", query = "SELECT g FROM Gimnasios g WHERE g.ruc = :ruc"),
    @NamedQuery(name = "Gimnasios.findByEstado", query = "SELECT g FROM Gimnasios g WHERE g.estado = :estado")})
public class Gimnasios implements Serializable {
    @OneToMany(mappedBy = "idGimnasio")
    private List<Ventas> ventasList;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_gimnasio")
    private Integer idGimnasio;
    @Size(max = 100)
    @Column(name = "razon_social")
    private String razonSocial;
    @Size(max = 120)
    @Column(name = "direccion")
    private String direccion;
    @Size(max = 20)
    @Column(name = "telefono")
    private String telefono;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 100)
    @Column(name = "email")
    private String email;
    @Size(max = 20)
    @Column(name = "ruc")
    private String ruc;
    @Column(name = "estado")
    private Integer estado;
    @OneToMany(mappedBy = "idGimnasio")
    private List<Estudiantes> estudiantesList;
    @OneToMany(mappedBy = "idGimnasio")
    private List<Producto> productoList;
    @OneToMany(mappedBy = "idGimnasio")
    private List<Usuarios> usuariosList;

    public Gimnasios() {
    }

    public Gimnasios(Integer idGimnasio) {
        this.idGimnasio = idGimnasio;
    }

    public Integer getIdGimnasio() {
        return idGimnasio;
    }

    public void setIdGimnasio(Integer idGimnasio) {
        this.idGimnasio = idGimnasio;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    @XmlTransient
    @JsonIgnore
    public List<Estudiantes> getEstudiantesList() {
        return estudiantesList;
    }

    public void setEstudiantesList(List<Estudiantes> estudiantesList) {
        this.estudiantesList = estudiantesList;
    }

    @XmlTransient
    @JsonIgnore
    public List<Producto> getProductoList() {
        return productoList;
    }

    public void setProductoList(List<Producto> productoList) {
        this.productoList = productoList;
    }

    @XmlTransient
    @JsonIgnore
    public List<Usuarios> getUsuariosList() {
        return usuariosList;
    }

    public void setUsuariosList(List<Usuarios> usuariosList) {
        this.usuariosList = usuariosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idGimnasio != null ? idGimnasio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Gimnasios)) {
            return false;
        }
        Gimnasios other = (Gimnasios) object;
        if ((this.idGimnasio == null && other.idGimnasio != null) || (this.idGimnasio != null && !this.idGimnasio.equals(other.idGimnasio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.controlgym.entities.Gimnasios[ idGimnasio=" + idGimnasio + " ]";
    }

    @XmlTransient
    @JsonIgnore
    public List<Ventas> getVentasList() {
        return ventasList;
    }

    public void setVentasList(List<Ventas> ventasList) {
        this.ventasList = ventasList;
    }
    
}
