/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.controlgym.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * @author broja
 */
@Entity
@Table(name = "estudiantes", catalog = "gym_control", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Estudiantes.findAll", query = "SELECT e FROM Estudiantes e"),
    @NamedQuery(name = "Estudiantes.findByFechaUltimoPago", query = "SELECT e FROM Estudiantes e WHERE e.fechaProximoPago = :fechaProximoPago"),
    @NamedQuery(name = "Estudiantes.findByIdEstudiante", query = "SELECT e FROM Estudiantes e WHERE e.idEstudiante = :idEstudiante"),
    @NamedQuery(name = "Estudiantes.findByNombreApellido", query = "SELECT e FROM Estudiantes e WHERE e.nombreApellido = :nombreApellido"),
    @NamedQuery(name = "Estudiantes.findByDireccion", query = "SELECT e FROM Estudiantes e WHERE e.direccion = :direccion"),
    @NamedQuery(name = "Estudiantes.findByTelefono", query = "SELECT e FROM Estudiantes e WHERE e.telefono = :telefono"),
    @NamedQuery(name = "Estudiantes.findByEmail", query = "SELECT e FROM Estudiantes e WHERE e.email = :email"),
    @NamedQuery(name = "Estudiantes.findByRuc", query = "SELECT e FROM Estudiantes e WHERE e.ruc = :ruc"),
    @NamedQuery(name = "Estudiantes.getEstudiantesPorVencerSinGym", query = "SELECT e FROM Estudiantes e WHERE e.fechaProximoPago <= :fechaValidacion and e.estado = 1"),
    @NamedQuery(name = "Estudiantes.getEstudiantesPorVencerConGym", query = "SELECT e FROM Estudiantes e WHERE e.fechaProximoPago <= :fechaValidacion and e.estado = 1 and e.idGimnasio.idGimnasio = :idGimnasio"),
    @NamedQuery(name = "Estudiantes.getEstudiantesByGimnasio", query = "SELECT e FROM Estudiantes e WHERE e.idGimnasio.idGimnasio = :idGimnasio ORDER BY e.estado DESC"),
    @NamedQuery(name = "Estudiantes.getEstudiantesActivosByGimnasio", query = "SELECT e FROM Estudiantes e WHERE e.idGimnasio.idGimnasio = :idGimnasio AND e.estado = 1"),
    @NamedQuery(name = "Estudiantes.findByEstado", query = "SELECT e FROM Estudiantes e WHERE e.estado = :estado")})
public class Estudiantes implements Serializable {

    @OneToMany(mappedBy = "idEstudiante")
    private List<Ventas> ventasList;
    private static final long serialVersionUID = 1L;
    @Column(name = "fecha_proximo_pago")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaProximoPago;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_estudiante")
    private Integer idEstudiante;
    @Size(max = 120)
    @Column(name = "nombre_apellido")
    private String nombreApellido;
    @Size(max = 120)
    @Column(name = "direccion")
    private String direccion;
    @Size(max = 20)
    @Column(name = "telefono")
    private String telefono;
    @Size(max = 50)
    @Column(name = "email")
    private String email;
    @Size(max = 20)
    @Column(name = "ruc")
    private String ruc;
    @Column(name = "estado")
    private Integer estado;
    @JoinColumn(name = "id_gimnasio", referencedColumnName = "id_gimnasio")
    @ManyToOne
    private Gimnasios idGimnasio;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idAlumno")
    private List<Asistencia> asistenciaList;

    public Estudiantes() {
    }

    public Estudiantes(Integer idEstudiante) {
        this.idEstudiante = idEstudiante;
    }

    public Date getFechaProximoPago() {
        return fechaProximoPago;
    }

    public void setFechaProximoPago(Date fechaProximoPago) {
        this.fechaProximoPago = fechaProximoPago;
    }

    public Integer getIdEstudiante() {
        return idEstudiante;
    }

    public void setIdEstudiante(Integer idEstudiante) {
        this.idEstudiante = idEstudiante;
    }

    public String getNombreApellido() {
        return nombreApellido;
    }

    public void setNombreApellido(String nombreApellido) {
        this.nombreApellido = nombreApellido;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public Gimnasios getIdGimnasio() {
        return idGimnasio;
    }

    public void setIdGimnasio(Gimnasios idGimnasio) {
        this.idGimnasio = idGimnasio;
    }
    
    @XmlTransient
    @JsonIgnore
    public List<Asistencia> getAsistenciaList() {
        return asistenciaList;
    }

    public void setAsistenciaList(List<Asistencia> asistenciaList) {
        this.asistenciaList = asistenciaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEstudiante != null ? idEstudiante.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Estudiantes)) {
            return false;
        }
        Estudiantes other = (Estudiantes) object;
        if ((this.idEstudiante == null && other.idEstudiante != null) || (this.idEstudiante != null && !this.idEstudiante.equals(other.idEstudiante))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.controlgym.entities.Estudiantes[ idEstudiante=" + idEstudiante + " ]";
    }

    @XmlTransient
    @JsonIgnore
    public List<Ventas> getVentasList() {
        return ventasList;
    }

    public void setVentasList(List<Ventas> ventasList) {
        this.ventasList = ventasList;
    }

}
