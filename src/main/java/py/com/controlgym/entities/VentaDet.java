/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.controlgym.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author broja
 */
@Entity
@Table(name = "venta_det", catalog = "gym_control", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VentaDet.findAll", query = "SELECT v FROM VentaDet v")
    ,
    @NamedQuery(name = "VentaDet.findByIdVentaDet", query = "SELECT v FROM VentaDet v WHERE v.idVentaDet = :idVentaDet")
    ,
    @NamedQuery(name = "VentaDet.findByCantidad", query = "SELECT v FROM VentaDet v WHERE v.cantidad = :cantidad")
    ,
    @NamedQuery(name = "VentaDet.findByFechaIns", query = "SELECT v FROM VentaDet v WHERE v.fechaIns = :fechaIns")
    ,
    @NamedQuery(name = "VentaDet.findByUsrUpd", query = "SELECT v FROM VentaDet v WHERE v.usrUpd = :usrUpd")
    ,
    @NamedQuery(name = "VentaDet.findByFechaUpd", query = "SELECT v FROM VentaDet v WHERE v.fechaUpd = :fechaUpd")})
public class VentaDet implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_venta_det")
    private Integer idVentaDet;
    @Column(name = "cantidad")
    private Integer cantidad;
    @Column(name = "valor_venta")
    private BigInteger valorVenta;
    @Column(name = "fecha_ins")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaIns;
    @Column(name = "usr_upd")
    private Integer usrUpd;
    @Column(name = "fecha_upd")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaUpd;
    @JoinColumn(name = "id_producto", referencedColumnName = "id_producto")
    @ManyToOne
    private Producto idProducto;
    @JoinColumn(name = "usr_ins", referencedColumnName = "id_usuario")
    @ManyToOne
    private Usuarios usrIns;
    @JoinColumn(name = "id_venta", referencedColumnName = "id_venta")
    @ManyToOne
    private Ventas idVenta;

    public VentaDet() {
    }

    public VentaDet(Integer idVentaDet) {
        this.idVentaDet = idVentaDet;
    }

    public Integer getIdVentaDet() {
        return idVentaDet;
    }

    public void setIdVentaDet(Integer idVentaDet) {
        this.idVentaDet = idVentaDet;
    }

    public BigInteger getValorVenta() {
        return valorVenta;
    }

    public void setValorVenta(BigInteger valorVenta) {
        this.valorVenta = valorVenta;
    }
    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Date getFechaIns() {
        return fechaIns;
    }

    public void setFechaIns(Date fechaIns) {
        this.fechaIns = fechaIns;
    }

    public Integer getUsrUpd() {
        return usrUpd;
    }

    public void setUsrUpd(Integer usrUpd) {
        this.usrUpd = usrUpd;
    }

    public Date getFechaUpd() {
        return fechaUpd;
    }

    public void setFechaUpd(Date fechaUpd) {
        this.fechaUpd = fechaUpd;
    }

    public Producto getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Producto idProducto) {
        this.idProducto = idProducto;
    }

    public Usuarios getUsrIns() {
        return usrIns;
    }

    public void setUsrIns(Usuarios usrIns) {
        this.usrIns = usrIns;
    }

    public Ventas getIdVenta() {
        return idVenta;
    }

    public void setIdVenta(Ventas idVenta) {
        this.idVenta = idVenta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idVentaDet != null ? idVentaDet.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VentaDet)) {
            return false;
        }
        VentaDet other = (VentaDet) object;
        if ((this.idVentaDet == null && other.idVentaDet != null) || (this.idVentaDet != null && !this.idVentaDet.equals(other.idVentaDet))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.controlgym.entities.VentaDet[ idVentaDet=" + idVentaDet + " ]";
    }

}
